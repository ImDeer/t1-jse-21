package t1.dkhrunina.tm.exception.field;

public class LoginEmptyException extends AbstractFieldException {

    public LoginEmptyException() {
        super("Error: login is empty");
    }

}