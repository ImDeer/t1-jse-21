package t1.dkhrunina.tm.command.system;

import t1.dkhrunina.tm.command.AbstractCommand;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    private static final String ARGUMENT = "-h";

    private static final String NAME = "help";

    private static final String DESCRIPTION = "Show available actions.";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("\n[HELP]");
        for (final AbstractCommand command : getCommandService().getTerminalCommands()) System.out.println(command);
    }

}