package t1.dkhrunina.tm.command;

import t1.dkhrunina.tm.api.model.ICommand;
import t1.dkhrunina.tm.api.service.IAuthService;
import t1.dkhrunina.tm.api.service.IServiceLocator;
import t1.dkhrunina.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract Role[] getRoles();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        final boolean hasName = name != null && !name.isEmpty();
        final boolean hasArgument = argument != null && !argument.isEmpty();
        final boolean hasDescription = description != null && !description.isEmpty();
        if (hasName) result += name;
        if (hasArgument) result += hasName ? (", " + argument) : argument;
        if (hasDescription) result += hasName || hasArgument ? (": " + description) : description;
        return result;
    }

}