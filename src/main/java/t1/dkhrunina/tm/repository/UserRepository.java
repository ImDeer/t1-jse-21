package t1.dkhrunina.tm.repository;

import t1.dkhrunina.tm.api.repository.IUserRepository;
import t1.dkhrunina.tm.enumerated.Role;
import t1.dkhrunina.tm.model.User;
import t1.dkhrunina.tm.util.HashUtil;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(final String login, final String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    public User create(String login, String password, String email) {
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public Boolean isEmailExist(final String email) {
        return findAll()
                .stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return findAll()
                .stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public User findByEmail(final String email) {
        return findAll()
                .stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst().orElse(null);
    }

    @Override
    public User findById(final String id) {
        return findAll()
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public User findByLogin(final String login) {
        return findAll()
                .stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst().orElse(null);
    }

}