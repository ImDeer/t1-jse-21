package t1.dkhrunina.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Status status : values()) {
            if (status.getDisplayName().equals(value)) return status;
        }
        return null;
    }

    public static String[] toValues() {
        final String[] values = new String[Status.values().length];
        int index = 0;
        for (Status status : Status.values()) {
            values[index] = status.getDisplayName();
            index++;
        }
        return values;
    }

    public String getDisplayName() {
        return displayName;
    }
}