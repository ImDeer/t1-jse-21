package t1.dkhrunina.tm.api.service;

import t1.dkhrunina.tm.api.repository.IRepository;
import t1.dkhrunina.tm.enumerated.Sort;
import t1.dkhrunina.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    List<M> findAll(Sort sort);

}