package t1.dkhrunina.tm.model;

import t1.dkhrunina.tm.api.model.IWBS;
import t1.dkhrunina.tm.enumerated.Status;

import java.util.Date;

public final class Task extends AbstractUserOwnedModel implements IWBS {

    private Date created = new Date();

    private String description = "";

    private String name = "";

    private String projectId;

    private Status status = Status.NOT_STARTED;

    public Task() {
    }

    public Task(final String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return name + (description.isEmpty() ? "" : (": " + description));
    }

}